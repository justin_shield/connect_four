﻿using System;
using NUnit.Framework;

namespace Connect_Four.Data.Tests
{
    [TestFixture]
    public class BoardTestFixture
    {
        private Board _board;

        [SetUp]
        public void Setup(){}

        [TearDown]
        public void TearDown()
        {
            _board = null;
        }

        
        [TestCase(-1, 4)]
        [TestCase(0, 4)]        
        [TestCase(1, 4)]       
        [TestCase(2, 4)]        
        [TestCase(3, 4)]
        [TestCase(4, -1)]
        [TestCase(4, 0)]
        [TestCase(4, 1)]
        [TestCase(4, 2)]
        [TestCase(4, 3)]        
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        [TestCase(3, 3)]
        public void TestInvalidBoardDimensions(int rows, int columns)
        {
            Assert.That(() => _board = new Board(rows, columns), Throws.TypeOf<ArgumentOutOfRangeException>());    
        }


        [TestCase(4, 4)]
        [TestCase(4, 5)]
        [TestCase(4, 6)]
        [TestCase(4, 7)]        
        [TestCase(5, 4)]
        [TestCase(6, 4)]
        [TestCase(7, 4)]
        [TestCase(5, 5)]
        [TestCase(6, 6)]
        [TestCase(7, 7)]
        [TestCase(8, 8)]
        [TestCase(9, 9)]
        public void TestValidBoardDimensions(int rows, int columns)
        {
            // Arrange

            // Act
            _board = new Board(rows, columns);

            // Assert
            Assert.That(() => _board.Rows == rows);
            Assert.That(() => _board.Columns == columns);

            for (var boardRow = 0; boardRow < rows; boardRow++)
            {
                for (var boardCol = 0; boardCol < columns; boardCol++)
                {
                    Assert.AreEqual(_board.Matrix[boardRow, boardCol], Checker.Empty);
                }
            }
        }


        [TestCase(4, 4, 0)]        
        [TestCase(4, 4, 1)]
        [TestCase(4, 4, 2)]
        [TestCase(4, 4, 3)]       
        public void TestValidCheckerPlacements(int rows, int columns, int checkerColumn)
        {
            // Arrange
            _board = new Board(rows, columns);
            var checker = Checker.Red;

            // Act

            // Assert
            for (var row = 0; row < rows; row++)
            {
                checker = checker == Checker.Red ? Checker.Yellow : Checker.Red;    // Swap the checker colour for each row
                Assert.AreEqual(row, _board.PlaceChecker(checker, checkerColumn));
                Assert.AreEqual(checker, _board.Matrix[row, checkerColumn]);
            }
            
        }

        [TestCase(4, 4, -1)]
        [TestCase(4, 4, 5)]
        [TestCase(4, 4, 6)]
        [TestCase(4, 4, 7)]
        public void TestInvalidCheckerPlacements(int rows, int columns, int checkerColumn)
        {
            // Arrange
            _board = new Board(rows, columns);
            var checker = Checker.Red;

            // Act

            // Assert
            for (var row = 0; row < rows; row++)
            {
                checker = checker == Checker.Red ? Checker.Yellow : Checker.Red;    // Swap the checker colour for each row

                // placeChecker returns -1 for an invalid placement, otherwise it returns the row number
                Assert.AreEqual(-1, _board.PlaceChecker(checker, checkerColumn));                
            }
        }

        [TestCase(4, 4, 0)]
        [TestCase(4, 4, 1)]
        [TestCase(4, 4, 2)]
        [TestCase(4, 4, 3)]
        public void TestInvalidCheckerPlacementColour(int rows, int columns, int checkerColumn)
        {
            // Arrange
            _board = new Board(rows, columns);           

            // Act

            // Assert
            for (var row = 0; row < rows; row++)
            {                                
                if (row == 0)                
                    Assert.AreEqual(0, _board.PlaceChecker(Checker.Red, checkerColumn)); // The first one will work but the next colour needs to be Yellow               
                else                
                    Assert.AreEqual(-1, _board.PlaceChecker(Checker.Red, checkerColumn));
                
            }
        }



    }
}

﻿
using System;
using System.ComponentModel;

namespace Connect_Four.Data
{
    public class Board
    {
        private readonly Checker[,] _matrix;
        private readonly int _rows;
        private readonly int _columns;

        private Checker _lastPlaced;

        public int Rows => _rows;
        public int Columns => _columns;
        public Checker[,] Matrix => _matrix;

        public Board(int rows, int columns)
        {
            if(rows < 4)
                throw new ArgumentOutOfRangeException("rows", "must be larger than 3");

            if(columns < 4)
                throw new ArgumentOutOfRangeException("columns", "must be larger than 3");

            _rows = rows;
            _columns = columns;
            _matrix = new Checker[_rows, _columns];

            InitializeBoard();
        }

        private void InitializeBoard()
        {
            for (var rowNum = 0; rowNum < _rows; rowNum++)
            {
                for (var colNum = 0; colNum < _columns; colNum++)
                {
                    _matrix[rowNum, colNum] = Checker.Empty;
                }
            }
        }

        /// <summary>
        /// Returns the row the checker has been placed into for that columns
        /// </summary>        
        /// <returns>Negative 1 if its unable to place a checker in that column</returns>
        public int PlaceChecker(Checker checker, int column)
        {            
            if(checker == Checker.Empty)
                throw new InvalidEnumArgumentException("Cannot place an empty checker");

            if (_lastPlaced == checker      // If the last placed checker is the same, then this is an invalid move
                || column < 0               // The column is less than 0 
                || column >= _columns)      // The column is greater than the num of columns
                return -1;

            // Cycle through the rows on that column
            for (var row = 0; row < _rows; row++)
            {
                if (_matrix[row, column] != Checker.Empty)  // Continue through the rows until we find an empty place
                    continue;

                _matrix[row, column] = checker;
                _lastPlaced = checker;
                return row;
            }

            // Cannot place another checker in that column
            return -1;
        }       

    }
}

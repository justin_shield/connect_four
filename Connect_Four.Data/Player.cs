﻿
namespace Connect_Four.Data
{
    public class Player
    {
        private readonly Checker _colour;
        private int _moveNumber;

        public Checker Colour => _colour;

        public Player(Checker colour)
        {
            _colour = colour;
        }

    }
}

﻿using System;
using System.ComponentModel;
using Connect_Four.Data;
using NUnit.Framework;

namespace Connect_Four.Domain.Tests
{
    [TestFixture]
    public class GameLogicTestFixture
    {
        private Board _board;        
        private ConnectFourLogic _logic;

        [SetUp]
        public void Setup()
        {
            _board = new Board(4,4);            
            _logic = new ConnectFourLogic();
        }

        [TearDown]
        public void TearDown()
        {            
        }

        [Test]
        public void CanCreateGameLogic()
        {
            // Arrange            

            // Act            

            // Assert
            Assert.IsNotNull(_logic);
        }

        [Test]        
        public void CheckForWin_NoBoard_ThrowsException()
        {
            // Arrange            

            // Act            

            // Assert
            Assert.That(() => _logic.CheckForWin(null, Checker.Empty), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void CheckForWin_EmptyChecker_ThrowsException()
        {
            // Arrange            

            // Act            

            // Assert
            Assert.That(() => _logic.CheckForWin(_board, Checker.Empty), Throws.TypeOf<InvalidEnumArgumentException>());
        }
        
        [TestCase(Checker.Yellow)]
        [TestCase(Checker.Red)]
        public void CheckForWin_Horizontal_ReturnsTrue(Checker checker)
        {
            // Arrange                        
            _board.Matrix[0, 0] = checker;
            _board.Matrix[0, 1] = checker;
            _board.Matrix[0, 2] = checker;
            _board.Matrix[0, 3] = checker;

            // Act

            // Assert
            CheckForWin(checker, _board);
        }

       

        [TestCase(Checker.Yellow)]
        [TestCase(Checker.Red)]
        public void CheckForWin_Vertical_ReturnsTrue(Checker checker)
        {
            // Arrange
            _board.Matrix[0, 0] = checker;
            _board.Matrix[1, 0] = checker;
            _board.Matrix[2, 0] = checker;
            _board.Matrix[3, 0] = checker;

            // Act

            // Assert
            CheckForWin(checker, _board);
        }
        

        [TestCase(Checker.Yellow)]
        [TestCase(Checker.Red)]
        public void CheckForWin_RightDiagonal_ReturnsTrue(Checker checker)
        {
            // Arrange
            _board.Matrix[0, 0] = checker;
            _board.Matrix[1, 1] = checker;
            _board.Matrix[2, 2] = checker;
            _board.Matrix[3, 3] = checker;

            // Act

            // Assert
            CheckForWin(checker, _board);
        }

        [TestCase(Checker.Yellow)]
        [TestCase(Checker.Red)]
        public void CheckForWin_LeftDiagonal_ReturnsTrue(Checker checker)
        {
            // Arrange
            _board.Matrix[3, 0] = checker;
            _board.Matrix[2, 1] = checker;
            _board.Matrix[1, 2] = checker;
            _board.Matrix[0, 3] = checker;

            // Act

            // Assert
            CheckForWin(checker, _board);
        }

        [Test]
        public void CheckForFull_Returns_False()
        {
            // Arrange

            // Act
            var isFull = _logic.BoardFull(_board);

            // Assert
            Assert.IsFalse(isFull, "Board should not be full");
        }
        
        [TestCase(Checker.Yellow)]
        [TestCase(Checker.Red)]
        public void CheckForFull_Returns_True(Checker checker)
        {
            // Arrange
            for (var row = 0; row < _board.Rows; row++)
            {
                for (var col = 0; col < _board.Columns; col++)
                {
                    _board.Matrix[row, col] = checker;
                }
            }

            // Act
            var isFull = _logic.BoardFull(_board);

            // Assert
            Assert.IsTrue(isFull, "Board should be full");
        }

        #region Helpers 

        private static Checker SwitchChecker(Checker checker)
        {
            return checker == Checker.Yellow ? Checker.Red : Checker.Yellow;
        }

        private void CheckForWin(Checker checker, Board board)
        {
            var otherChecker = SwitchChecker(checker);

            // Act
            var winCondition = _logic.CheckForWin(board, checker);
            var loseCondition = _logic.CheckForWin(board, otherChecker);

            // Assert
            Assert.IsTrue(winCondition, "{0} should win", checker);
            Assert.IsFalse(loseCondition, "{0} should lose", otherChecker);
        }

        #endregion



    }
}

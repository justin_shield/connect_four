﻿using System;
using System.Collections.Generic;
using Connect_Four.Data;

namespace Connect_Four.Domain
{
   

    /// <summary>
    /// Class contains the current game state
    /// </summary>
    public class Game
    {
        private readonly IGameLogic _logic;

        private int _playerTurn;
        private Board _board;
        private List<Player> _players;
        private Player _currentPlayer;

        private bool _isFinished = true;

        public Game(IGameLogic logic)
        {
            _logic = logic;
        }

        public State Start(int rows, int columns)
        {
            _players = new List<Player>
            {
                new Player(Checker.Yellow),
                new Player(Checker.Red)
            };

            _board = new Board(rows, columns);
            _playerTurn = -1;
            _isFinished = false;

            return NewTurn();
        }

        private State NewTurn()
        {
            // Set the player turn
            _playerTurn++;
            if (_playerTurn >= _players.Count)
                _playerTurn = 0;

            // Set the Current Player
            _currentPlayer = _players[_playerTurn];

            return new State
            {
                Board = _board,
                Player = _currentPlayer.Colour.ToString(),
                Message = string.Empty,
                IsFinished = _isFinished
            };
        }
        
        public State PlaceChecker(int column)
        {
            try
            {
                if (_board.PlaceChecker(_currentPlayer.Colour, column) < 0)                
                    throw new ArgumentOutOfRangeException("Invalid column number");                                
            }
            catch (Exception ex)
            {
                return CreateState(ex.Message);
            }

            if (_logic.CheckForWin(_board, _currentPlayer.Colour))
            {
                _isFinished = true;
                return CreateState($"{_currentPlayer.Colour} WINS!");
            }

            if (_logic.CheckForDraw(_board))
            {
                _isFinished = true;
                return CreateState($"Draw!");
            }

            return NewTurn();
        }       


        private State CreateState(string message = "")
        {
            return new State
            {
                Board = _board,
                Message = message,
                IsFinished = _isFinished,
                Player = _currentPlayer.Colour.ToString()
            };
        }


    }
}

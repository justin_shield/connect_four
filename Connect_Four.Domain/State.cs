﻿using Connect_Four.Data;

namespace Connect_Four.Domain
{
    /// <summary>
    /// DTO class for transmitting and storing the current game state
    /// </summary>
    public class State
    {
        public bool IsFinished { get; set; }
        public Board Board { get; set; }
        public string Player { get; set; }
        public string Message { get; set; }
    }
}

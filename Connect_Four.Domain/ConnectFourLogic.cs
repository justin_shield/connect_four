﻿using System;
using System.ComponentModel;
using System.Text;
using Connect_Four.Data;

namespace Connect_Four.Domain
{
    public class ConnectFourLogic : IGameLogic
    {
        private const string WinCondition = "tttt";

        public bool CheckForDraw(Board board)
        {
            return !CheckForWin(board, Checker.Yellow)
                   && !CheckForWin(board, Checker.Red)
                   && BoardFull(board);
        }

        /// <summary>
        /// Returns true if the selected colour checker wins 4 in a row
        /// </summary>
        public bool CheckForWin(Board board, Checker checker)
        {
            if(board == null)
                throw new ArgumentNullException("board", "cannot be null");

            if(checker == Checker.Empty)
                throw new InvalidEnumArgumentException("Checker cannot be empty");

            // Check for an invalid board
            if (board.Rows <= 0 || board.Columns <= 0)
                return false;
            
            if (CheckRows(board, checker))
                return true;
           
            if (CheckColumns(board, checker))
                return true;
            
            if (CheckDiagonals(board, checker))
                return true;

            return false;
        }

        /// <summary>
        /// Returns true if there are no empty places left on the board
        /// </summary>        
        public bool BoardFull(Board board)
        {            
            for (var row = 0; row < board.Rows; row++)
            {                
                for (var col = 0; col < board.Columns; col++)
                {
                    if (CheckLocation(board, row, col, Checker.Empty))
                        return false;
                }
            }

            return true;
        }

        // Returns true of the selected checker is in that board location
        // Returns false if the position is out of bounds
        private static bool CheckLocation(Board board, int row, int column, Checker checker)
        {            
            if (row < 0 || row >= board.Rows || column < 0 || column >= board.Columns)
                return false;

            return board.Matrix[row, column] == checker;
        }

        // Returns true if the selected checker is 4 in a row
        private bool CheckRows(Board board, Checker checker)
        {
            var rowsString = new StringBuilder();
            for (var row = 0; row < board.Rows; row++)
            {
                rowsString.Clear();
                for (var col = 0; col < board.Columns; col++)
                {
                    rowsString.Append(CheckLocation(board, row, col, checker) ? "t" : "f");
                }

                if (rowsString.ToString().Contains(WinCondition))
                    return true;
            }

            return false;
        }

        // Returns true if the selected checker is 4 in a column
        private static bool CheckColumns(Board board, Checker checker)
        {
            var colString = new StringBuilder();

            for (var col = 0; col < board.Columns; col++)
            {
                colString.Clear();

                for (var row = 0; row < board.Rows; row++)
                {
                    colString.Append(CheckLocation(board, row, col, checker) ? "t" : "f");
                }

                if (colString.ToString().Contains(WinCondition))
                    return true;
            }
            return false;
        }

        // Returns true if the selected checker is 4 in a diagonal either left or right
        private static bool CheckDiagonals(Board board, Checker checker)
        {
            var diagsRight = new StringBuilder();
            var diagsLeft = new StringBuilder();

            for (var row = 0; row < board.Rows; row++)
            {
                for (var column = 0; column < board.Columns; column++)
                {
                    diagsRight.Clear();
                    diagsLeft.Clear();
                    for (var i = 0; i < 4; i++)
                    {
                        diagsRight.Append(CheckLocation(board, row + i, column + i, checker) ? "t" : "f");
                        diagsLeft.Append(CheckLocation(board, row + i, column - i, checker) ? "t" : "f");
                    }

                    if (diagsRight.ToString().Contains(WinCondition)
                     || diagsLeft.ToString().Contains(WinCondition))
                        return true;
                }
            }
            return false;
        }
    

    }
}

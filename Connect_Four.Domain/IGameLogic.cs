﻿using Connect_Four.Data;

namespace Connect_Four.Domain
{
    public interface IGameLogic
    {
        bool CheckForDraw(Board board);
        bool CheckForWin(Board board, Checker checker);
    }
}

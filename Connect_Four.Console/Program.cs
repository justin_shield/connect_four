﻿using System;
using System.Text;
using Connect_Four.Data;
using Connect_Four.Domain;

namespace Connect_Four.App
{
    class Program
    {
        private static Game _connectFour;
        private static State _gameState;

        static void Main(string[] args)
        {
            while (true)
            {
                NewGame();
               
                if (!PlayAgain())
                    break;                
            }

            Console.WriteLine("Thanks for playing, press any key to exit");
            Console.ReadKey();
        }       



        private static void NewGame()
        {
            Console.WriteLine("Please enter the board dimensions (number of rows, number of columns)");
            var dimensions = Console.ReadLine().Split(' ');

            try
            {
                var rows = Convert.ToInt32(dimensions[0]);
                var cols = Convert.ToInt32(dimensions[1]);

                _connectFour = new Game(new ConnectFourLogic());
                _gameState = _connectFour.Start(rows, cols);
            }
            catch (ArgumentOutOfRangeException oor)
            {
                Console.WriteLine(oor.Message);
                NewGame();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to parse one or more parameters");
                NewGame();
            }

            while (!_gameState.IsFinished)
            {                
                DisplayBoard(_gameState);
                DisplayTurn(_gameState);

                int col = 0;

                try
                {
                    col = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Unable to parse column");
                    continue;
                }
               
                var state = _connectFour.PlaceChecker(col-1);

                if (state.IsFinished)
                    DisplayBoard(state);

                _gameState = state;

                Console.WriteLine(_gameState.Message);
            }

            
        }

        private static bool PlayAgain()
        {
            Console.WriteLine("Game Over!");
            Console.WriteLine("Do you wish to play again? Y/N?");                                                        
            return Console.ReadLine().StartsWith("y", StringComparison.OrdinalIgnoreCase);
        }

        private static void DisplayBoard(State gameState)
        {
            var board = gameState.Board;
            var output = new StringBuilder();
            for (var row = board.Rows - 1; row >= 0; row--)
            {
                output.Clear();
                for (var col = 0; col < board.Columns; col++)
                {
                    var cell = board.Matrix[row, col];
                    var cellChar = cell == Checker.Empty
                        ? "o"
                        : cell == Checker.Yellow
                            ? "y"
                            : "r";

                    output.Append(cellChar);
                }

                Console.WriteLine(string.Join("\t\t", output.ToString()));
            }
        }

        private static void DisplayTurn(State gameState)
        {
            Console.WriteLine("{0}s turn:", gameState.Player);
        }

    }
}
